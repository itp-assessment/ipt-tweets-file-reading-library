﻿using FluentAssertions;
using ITP.FileReader.Adaptors;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class JsonTweetsDeserializerTest
    {
        [Fact]
        public void Deserialize_TextToTweets_Correctly()
        {            
            var sut = new JsonTweetsDeserializer();
            var result = sut.Deserialize(
                @"{
                  ""tweets"": [
                        {
                          ""Author"": ""Udi Dahan"",
                          ""likeCount"": 20,
                          ""text"": ""I am a software simplist""
                        },
                        {
                          ""Author"": ""Uncle Bob"",
                          ""likeCount"": 100,
                          ""text"": ""TDD is the key""
                        }
                  ]
                }");

            result.Should().BeEquivalentTo(new[]
            {
                new Tweet { Author= "Udi Dahan", LikeCount=20, Text="I am a software simplist" },
                new Tweet { Author= "Uncle Bob", LikeCount=100, Text="TDD is the key" },
            });
        }
    }
}
