﻿using FluentAssertions;
using ITP.FileReader.Adaptors;
using ITP.FileReader.Encryption;
using System.Text;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class TweetXmlFileReaderTest
    {
        [Fact]
        public void Load_XmlTweetFile_ShouldExtractTweetsFromTheFile()
        {
            ITweetFileReader sut = CreateTweetTextFileReader();

            var result = sut.Read("./tweets.xml");

            var expectedTweets = new[]
            {
                new Tweet { Author= "Udi Dahan", LikeCount=20, Text="I am a software simplist" },
                new Tweet { Author= "Uncle Bob", LikeCount=100, Text="TDD is the key" },
            };
            result.HasFailed.Should().BeFalse();
            result.Tweets.Should().BeEquivalentTo(expectedTweets);
            result.Error.Should().BeNull();
        }

        [Fact]
        public void Load_UnexistingFile_ShouldReturnAnError()
        {
            ITweetFileReader sut = CreateTweetTextFileReader();

            var result = sut.Read("./unexisting-tweets-file.xml");

            result.HasFailed.Should().BeTrue();
            result.Error.Should().NotBeNullOrEmpty();
        }

       private TweetFileReader CreateTweetTextFileReader()
            => new TweetFileReader(
                new TextFileReader(Encoding.UTF8),
                new XmlTweetsDeserializer());

        // E2E test to validate US
        [Fact]
        public void Ensure_EncryptedXmlFile_IsWellRead()
        {
            var tweetFileReader = new TweetFileReader(
                 new TextFileReader(Encoding.UTF8).WithReverseDecryption(),
                new XmlTweetsDeserializer());

            // reverse-tweet.txt is the result of the file tweets.xml reversed by an oline tool
            // extension has been changed to avoid VS warnings because the file has a malformed xml content
            var result = tweetFileReader.Read("reverse-tweets.xml.txt");

            var expectedTweets = new[]
            {
                new Tweet { Author= "Udi Dahan", LikeCount=20, Text="I am a software simplist" },
                new Tweet { Author= "Uncle Bob", LikeCount=100, Text="TDD is the key" },
            };
            result.HasFailed.Should().BeFalse();
            result.Tweets.Should().BeEquivalentTo(expectedTweets);
            result.Error.Should().BeNull();
        }
    }
}
