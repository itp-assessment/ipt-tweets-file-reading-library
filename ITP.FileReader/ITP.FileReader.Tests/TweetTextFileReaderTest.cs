using FluentAssertions;
using ITP.FileReader.Adaptors;
using System.Text;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class TweetTextFileReaderTest
    {
        const char Delimiter = ';';
        [Fact]
        public void Load_TxtTweetFile_ShouldExtractTweetsFromTheFile()
        {
            ITweetFileReader sut = CreateTweetTextFileReader();

            var result = sut.Read("./tweets.txt");

            var expectedTweets = new[]
            {
                new Tweet { Author= "Udi Dahan", LikeCount=20, Text="I am a software simplist" },
                new Tweet { Author= "Uncle Bob", LikeCount=100, Text="TDD is the key" },
            };
            result.HasFailed.Should().BeFalse();
            result.Tweets.Should().BeEquivalentTo(expectedTweets);
            result.Error.Should().BeNull();
        }

        [Fact]
        public void Load_UnexistingFile_ShouldReturnAnError()
        {
            ITweetFileReader sut = CreateTweetTextFileReader();

            var result = sut.Read("./unexisting-tweets-file.txt");

            result.HasFailed.Should().BeTrue();
            result.Error.Should().NotBeNullOrEmpty();
        }

        private TweetFileReader CreateTweetTextFileReader()
            => new TweetFileReader(
                new TextFileReader(Encoding.UTF8), 
                new CsvTweetsDeserializer(Delimiter));
    }
}
