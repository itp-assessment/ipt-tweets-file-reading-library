﻿using FluentAssertions;
using ITP.FileReader.Authorization;
using Moq;
using System;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class RestrictedFileReaderTest
    {
        private readonly Mock<ITextFileReader> _fileReaderStub;
        private readonly Mock<IFileAuthorizationService> _authorizationServiceStub;

        public RestrictedFileReaderTest()
        {
            _fileReaderStub = new Mock<ITextFileReader>();
            _fileReaderStub.Setup(x => x.Read("fake-file")).Returns("Hello");
            _authorizationServiceStub = new Mock<IFileAuthorizationService>();
        }
        [Fact]
        public void Read_UserAllowed_ReturnedFileContent()
        {

            _authorizationServiceStub.Setup(x => x.GrantsAccessTo("fake-file")).Returns(true);

            var sut = new RestrictedFileReader(
                _fileReaderStub.Object, 
                _authorizationServiceStub.Object);

            var result = sut.Read("fake-file");

            result.Should().Be("Hello");
        }

        [Fact]
        public void Read_UserNotAllowed_ThrowsException()
        {

            _authorizationServiceStub.Setup(x => x.GrantsAccessTo("fake-file")).Returns(false);

            var sut = new RestrictedFileReader(
                _fileReaderStub.Object, 
                _authorizationServiceStub.Object);

            sut.Invoking(s => s.Read("fake-file"))
                .Should()
                .Throw<UnauthorizedAccessException>();
        }
    }
}
