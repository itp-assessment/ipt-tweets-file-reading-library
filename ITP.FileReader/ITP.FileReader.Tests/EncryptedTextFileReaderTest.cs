﻿using FluentAssertions;
using ITP.FileReader.Encryption;
using Moq;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class EncryptedTextFileReaderTest
    {
        [Fact]
        public void Read_ShouldDecryptTheValue_ReturnedByDecoratedReader()
        {
            var txtFileReaderStub = new Mock<ITextFileReader>();
            txtFileReaderStub.Setup(x => x.Read("fake-file")).Returns("Hello-encrypted");

            var decryptorStub = new Mock<ITextDecryptor>();
            decryptorStub.Setup(x => x.Decrypt("Hello-encrypted")).Returns("Hello");

            var sut = new EncryptedTextFileReader(txtFileReaderStub.Object, decryptorStub.Object);

            var result = sut.Read("fake-file");

            result.Should().Be("Hello");
        }
    }
}
