﻿using FluentAssertions;
using ITP.FileReader.Authorization;
using System;
using System.Collections.Generic;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class RoleBasedAuthorizationServiceTest
    {
        private Dictionary<string, IEnumerable<FilePermission>> RolePermissions
            => new Dictionary<string, IEnumerable<FilePermission>>
            {
                {"admin", new []{ new FilePermission("read", "*") } },
                {"user", new[]
                    {
                        new FilePermission("read", "user-file"),
                        new FilePermission("delete", "other-file")
                    } 
                }
            };
       
        [Fact]
        public void GrantsAccess_RoleHasReadAccessOnStar_AlwaysTrue()
        {
            Func<string> getCurrentUserRole = () => "admin";
            var sut = new RoleBasedAuthorizationService(getCurrentUserRole, GetPermissions);

            var userFileResult = sut.GrantsAccessTo("user-file");
            var otherFileReesult = sut.GrantsAccessTo("other-file");
            userFileResult.Should().Be(true);
            otherFileReesult.Should().Be(true);
        }
        [Fact]
        public void GrantsAccess_RoleHasReadAccessOnSpecificFile_FalseForOtherFiles()
        {
            Func<string> getCurrentUserRole = () => "user";
            var sut = new RoleBasedAuthorizationService(getCurrentUserRole, GetPermissions);

            var userFileResult = sut.GrantsAccessTo("user-file");
            var otherFileReesult = sut.GrantsAccessTo("other-file");
            userFileResult.Should().Be(true);
            otherFileReesult.Should().Be(false);
        }

        private IEnumerable<FilePermission> GetPermissions(string role)
            => RolePermissions[role];
    }
}
