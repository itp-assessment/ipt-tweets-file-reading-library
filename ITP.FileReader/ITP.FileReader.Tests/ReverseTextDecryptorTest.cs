﻿using FluentAssertions;
using ITP.FileReader.Encryption;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class ReverseTextDecryptorTest
    {
        [Theory]
        [InlineData("Hello", "olleH")]
        [InlineData("AAA", "AAA")]
        public void Read_ShouldReverseTheValueReceived(string text, string expectedResult)
        {
            var decryptor = new ReverseTextDecryptor();

            var result = decryptor.Decrypt(text);

            result.Should().Be(expectedResult);
        }
    }
}
