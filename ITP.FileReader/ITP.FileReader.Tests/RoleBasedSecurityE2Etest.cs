﻿using FluentAssertions;
using ITP.FileReader.Adaptors;
using ITP.FileReader.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ITP.FileReader.Tests
{
    public class RoleBasedSecurityE2Etest
    {
        // E2E test to validate US
        [Fact]
        public void Ensure_PO_IsNotAllowedTo_ReadTweetsTxtFile()
        {
            TweetFileReader reader = CreateTextReaderWithUserRole("po");

            var result = reader.Read("tweets.txt");

            result.HasFailed.Should().BeTrue();
        }
        [Fact]
        public void Ensure_Dev_IsAllowedTo_ReadTweetsTxtFile()
        {
            TweetFileReader reader = CreateTextReaderWithUserRole("dev");

            var result = reader.Read("tweets.txt");

            var expectedTweets = new[]
            {
                new Tweet { Author= "Udi Dahan", LikeCount=20, Text="I am a software simplist" },
                new Tweet { Author= "Uncle Bob", LikeCount=100, Text="TDD is the key" },
            };
            result.HasFailed.Should().BeFalse();
            result.Tweets.Should().BeEquivalentTo(expectedTweets);
            result.Error.Should().BeNull();
        }

        private static TweetFileReader CreateTextReaderWithUserRole(string role)
        {
            var rolePermissions = new Dictionary<string, IEnumerable<FilePermission>>
            {
                {"dev", new []{ new FilePermission("read", "tweets.txt") } },
                {"po", Enumerable.Empty<FilePermission>() }
            };

            var reader = new TweetFileReader(
                new TextFileReader(Encoding.UTF8)
                    .WithRoleBasedAuthorization(
                        () => role,
                        role => rolePermissions[role]),
                new CsvTweetsDeserializer(';'));
            return reader;
        }
    }
}
