﻿using System;
using System.Linq;

namespace ITP.FileReader
{
    public class TweetFileReader : ITweetFileReader
    {
        private readonly ITextFileReader _fileReader;
        private readonly ITweetsDeserializer _adaptor;
        public TweetFileReader(ITextFileReader textFileReader, ITweetsDeserializer tweetsDeserializer)
        {
            _fileReader = textFileReader ?? throw new ArgumentNullException(nameof(textFileReader));
            _adaptor = tweetsDeserializer ?? throw new ArgumentNullException(nameof(tweetsDeserializer));
        }
        public ReadResult Read(string filePath)
        {
            try
            {
                var text = _fileReader.Read(filePath);
                return _adaptor.Deserialize(text).ToList();
            }
            catch (Exception ex)
            {
                return ReadResult.Failure(ex.ToString());
            }
        }
    }
}
