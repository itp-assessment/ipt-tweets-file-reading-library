﻿using System.Collections.Generic;

namespace ITP.FileReader
{
    public interface ITweetsDeserializer
    {
        IEnumerable<Tweet> Deserialize(string text);
    }
}
