﻿namespace ITP.FileReader
{
    public class Tweet
    {
        public string Author { get; set; }
        public string Text { get; set; }
        public uint LikeCount { get; set; }
    }
}
