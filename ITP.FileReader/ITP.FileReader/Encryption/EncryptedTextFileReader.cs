﻿using System;

namespace ITP.FileReader.Encryption
{
    public class EncryptedTextFileReader : ITextFileReader
    {
        private readonly ITextFileReader _inner;
        private readonly ITextDecryptor _decryptor;

        public EncryptedTextFileReader(ITextFileReader inner, ITextDecryptor decryptor)
        {
            _inner = inner ?? throw new ArgumentNullException(nameof(inner));
            _decryptor = decryptor ?? throw new ArgumentNullException(nameof(decryptor));
        }
        public string Read(string path)
            => _decryptor.Decrypt(_inner.Read(path));
    }
}
