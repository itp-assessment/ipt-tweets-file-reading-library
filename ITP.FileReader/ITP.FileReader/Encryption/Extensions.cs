﻿namespace ITP.FileReader.Encryption
{
    public static class Extensions
    {
        public static ITextFileReader WithDecryption(this ITextFileReader inner, ITextDecryptor decryptor)
            => new EncryptedTextFileReader(inner, decryptor);
        public static ITextFileReader WithReverseDecryption(this ITextFileReader inner)
            => new EncryptedTextFileReader(inner, new ReverseTextDecryptor());
    }
}
