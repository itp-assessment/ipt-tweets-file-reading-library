﻿using System;

namespace ITP.FileReader.Encryption
{
    public class ReverseTextDecryptor : ITextDecryptor
    {
        public string Decrypt(string encryptedText)
        {
            var charArray = encryptedText.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
