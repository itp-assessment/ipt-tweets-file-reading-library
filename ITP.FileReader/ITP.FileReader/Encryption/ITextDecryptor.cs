﻿namespace ITP.FileReader.Encryption
{
    public interface ITextDecryptor
    {
        string Decrypt(string encryptedText);
    }
}
