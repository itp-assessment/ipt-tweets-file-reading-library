﻿using System.Collections.Generic;
using System.Xml;

namespace ITP.FileReader.Adaptors
{
    public class XmlTweetsDeserializer : ITweetsDeserializer
    {        
        public IEnumerable<Tweet> Deserialize(string text)
        {
            var document = new XmlDocument();
            document.LoadXml(text);
            return Parse(document.GetElementsByTagName("tweet"));
        }
        private List<Tweet> Parse(XmlNodeList elements)
        {
            var result = new List<Tweet>();
            for (int i = 0; i < elements.Count; i++)
            {
                result.Add(Parse(elements[i] as XmlElement));
            }
            return result;
        }

        private Tweet Parse(XmlElement element)
            => new Tweet
            {
                Author = element.GetAttribute("author"),
                LikeCount = uint.Parse(element.GetAttribute("likeCount")),
                Text = element.InnerText
            };
    }

}
