﻿using System.Collections.Generic;
using System.Text.Json;

namespace ITP.FileReader.Adaptors
{
    public class JsonTweetsDeserializer : ITweetsDeserializer
    {

        public IEnumerable<Tweet> Deserialize(string text)
            => JsonSerializer.Deserialize<JsonFormat>(
                text, 
                new JsonSerializerOptions {PropertyNameCaseInsensitive = true})
            .Tweets;
        private class JsonFormat
        {
            public IEnumerable<Tweet> Tweets { get; set; }
        }
    }
}
