﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ITP.FileReader.Adaptors
{
    public class CsvTweetsDeserializer : ITweetsDeserializer
    {
        private readonly char _separator;
        public CsvTweetsDeserializer(char separator)
        {
            _separator = separator;
        }
        public IEnumerable<Tweet> Deserialize(string text)
        {
            var lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            return lines.Select(Parse).ToList();
        }
        private Tweet Parse(string textLine)
        {
            var fields = textLine.Split(_separator);
            return new Tweet
            {
                Author = fields[0],
                Text = fields[1],
                LikeCount = uint.Parse(fields[2]),
            };
        }
    }

}
