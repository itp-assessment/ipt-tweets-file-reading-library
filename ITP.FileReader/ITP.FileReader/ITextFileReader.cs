﻿namespace ITP.FileReader
{
    public interface ITextFileReader
    {
        string Read(string path);
    }
}
