﻿using System.Collections.Generic;
using System.Linq;

namespace ITP.FileReader
{
    public class ReadResult
    {
        private ReadResult(IEnumerable<Tweet> tweets)
        {
            Tweets = tweets ?? Enumerable.Empty<Tweet>();
            Error = null;
        }
        private ReadResult(string error)
        {
            Tweets = null;
            Error = string.IsNullOrEmpty(error) ? "Unknown" : error;
        }
        public string Error { get; }
        public bool HasFailed => !string.IsNullOrEmpty(Error);
        public IEnumerable<Tweet> Tweets { get; }

        public static ReadResult Failure(string error) => new ReadResult(error);
        public static ReadResult Success(IEnumerable<Tweet> tweets) => new ReadResult(tweets);
        public static implicit operator ReadResult(List<Tweet> tweets) => new ReadResult(tweets);
    }
}
