﻿using System.IO;
using System.Text;

namespace ITP.FileReader
{
    public class TextFileReader : ITextFileReader
    {
        private readonly Encoding _encoding;

        public TextFileReader(Encoding encoding)
        {
            _encoding = encoding ?? Encoding.UTF8;
        }
        public string Read(string path)
            => File.ReadAllText(path, _encoding);
    }
}
