﻿namespace ITP.FileReader
{
    public interface ITweetFileReader
    {
        ReadResult Read(string filePath);
    }
}
