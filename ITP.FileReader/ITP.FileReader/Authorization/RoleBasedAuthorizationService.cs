﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ITP.FileReader.Authorization
{
    public class RoleBasedAuthorizationService : IFileAuthorizationService
    {
        private readonly Func<string> _getCurrentUserRole;
        private readonly Func<string, IEnumerable<FilePermission>> _getRolePermissions;

        public RoleBasedAuthorizationService(Func<string> userRoleProvider, Func<string, IEnumerable<FilePermission>> getRolePermissions)
        {
            _getCurrentUserRole = userRoleProvider ?? throw new ArgumentNullException(nameof(userRoleProvider));
            _getRolePermissions = getRolePermissions ?? throw new ArgumentNullException(nameof(getRolePermissions));
        }
        // very basic implementation
        public bool GrantsAccessTo(string filePath)
        {
            var role = _getCurrentUserRole();
            var permissions = _getRolePermissions(role);

            return permissions
                .Where(perm => perm.Name == "read")
                .Any(perm => perm.File == "*" || perm.File.Equals(filePath, StringComparison.InvariantCulture));
        }
    }
}
