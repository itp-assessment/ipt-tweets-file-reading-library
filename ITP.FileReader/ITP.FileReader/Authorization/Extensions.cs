﻿using System;
using System.Collections.Generic;

namespace ITP.FileReader.Authorization
{
    public static class Extensions
    {
        public static ITextFileReader WithAuthorization(this ITextFileReader inner, IFileAuthorizationService fileAuthorizationService)
            => new RestrictedFileReader(inner, fileAuthorizationService);
        public static ITextFileReader WithRoleBasedAuthorization(
            this ITextFileReader inner, 
            Func<string> userRoleProvider, 
            Func<string, IEnumerable<FilePermission>> getRolePermissions
            )
            => WithAuthorization(
                inner, 
                new RoleBasedAuthorizationService(userRoleProvider, getRolePermissions));
    }
}
