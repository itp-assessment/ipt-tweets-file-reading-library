﻿namespace ITP.FileReader.Authorization
{
    public interface IFileAuthorizationService
    {
        bool GrantsAccessTo(string filePath);
    }
}
