﻿namespace ITP.FileReader.Authorization
{
    
    public class FilePermission
    {
        /// <summary>
        /// Define a permission that can be granted to an entity on a dedicated file
        /// </summary>
        /// <param name="name">name of the permission. Ex: read, write, ... </param>
        /// <param name="file">the path of the file or * for every files</param>
        public FilePermission(string name, string file)
        {
            Name = name;
            File = file;
        }
        public string Name { get; }
        public string File { get; }
    }
}
