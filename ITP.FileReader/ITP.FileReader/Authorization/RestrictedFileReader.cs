﻿using System;

namespace ITP.FileReader.Authorization
{
    public class RestrictedFileReader : ITextFileReader
    {
        private readonly ITextFileReader _inner;
        private readonly IFileAuthorizationService _authorisationService;

        public RestrictedFileReader(ITextFileReader inner, IFileAuthorizationService authorizationService)
        {
            _inner = inner ?? throw new ArgumentNullException(nameof(inner));
            _authorisationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
        }
        public string Read(string path)
        {
            if (!_authorisationService.GrantsAccessTo(path))
            {
                throw new UnauthorizedAccessException($"The current user is not allowed to access the file {path}");
            }
            return _inner.Read(path);
        }
    }
}
